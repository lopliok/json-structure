import React from "react"
import { OperationProps } from "./RenderTree";


export const RenderTextAndNumbers: React.FunctionComponent<OperationProps> = ({ itemKey, value}: OperationProps) => {
    console.log(typeof value)

    const isString = typeof value === "string"

    return (
      <span style={{ color: isString ? "green" : "blue"}}>     
         {isString ? `"${value}"` : value}
      </span>
    );
  }