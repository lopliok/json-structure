import React from "react";
import RenderTree, { OperationProps } from "./RenderTree";
import { isObject } from "util";

export const RenderObject: React.FunctionComponent<OperationProps> = React.memo(
  ({ itemKey, value }: OperationProps) => {
    const [open, setOpen] = React.useState(true);

    const keyIsNumber = isNaN(Number(itemKey)) && itemKey;

    // console.log(itemKey)
    return (
      <div style={{ position: "relative" }}>
        <span
          style={{ position: "absolute", left: -15 }}
          onClick={() => setOpen(!open)}
        >
          {!open ? "+" : "-"}
        </span>
        {keyIsNumber && <span style={{ fontWeight: "bold" }}>{itemKey}</span>}
        <span style={{ paddingLeft: keyIsNumber ? 15 : 0 }}>{`{ `}</span>
        {!open && <span>{` }`}</span>}
        <div style={{ paddingLeft: 20 }}>
          {open &&
            (Object.keys(value) as Array<keyof typeof value>).map(ObjKey => {
              return (
                <div key={`key-${ObjKey}`}>
                  <span style={{ fontWeight: "bold" }}>
                    {!isObject(value[ObjKey]) && `${ObjKey}: `}
                  </span>
                  <RenderTree itemKey={ObjKey} value={value[ObjKey]} />
                </div>
              );
            })}
        </div>
        {open && <span>{`}`}</span>}
      </div>
    );
  }
);
