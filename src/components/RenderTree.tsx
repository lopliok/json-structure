import React from "react"
import { RenderObject } from "./RenderObject";
import { RenderArray } from "./RenderArray";
import { RenderTextAndNumbers } from "./RenderTextAndNumber";
import { isObject } from "util";

type NestedRuleItem = [] | string | object;

type RuleItem = NestedRuleItem[] | string | object;


export type OperationProps = {
    itemKey: string;
    value: RuleItem;
  };


  type Rule = {
    check: (value: RuleItem) => boolean;
    type: string;
    operation: React.FunctionComponent<OperationProps>;
  };
  

const rules: Rule[] = [
    {
      check: (value: RuleItem) => typeof value === "number",
      type: "number",
      operation: RenderTextAndNumbers
    },
    {
      check: (value: RuleItem) => typeof value === "string",
      type: "string",
      operation: RenderTextAndNumbers
    },
    {
      check: (value: RuleItem) =>
      value === Object(value) && !Array.isArray(value),
      type: "object",
      operation: RenderObject
    },
    {
      check: (value: RuleItem) => Array.isArray(value),
      type: "array",
      operation: RenderArray
    }
  ];

type RenderTreeProps = {
    itemKey: string;
     value: RuleItem
}
  

const RenderTree: React.FunctionComponent<RenderTreeProps> = ({itemKey, value}: RenderTreeProps) => {
    const rule = rules.find(it => it.check(value));
    const Component = rule && rule.operation;

    return (
      <React.Fragment key={`key-${itemKey}`}>
        {Component && <Component itemKey={itemKey} value={value} />}
      </React.Fragment>
    );
  };

  export default RenderTree;