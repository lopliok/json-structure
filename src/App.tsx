import * as React from "react";

import { data } from "./data";

import "./App.css";
import RenderTree from "./components/RenderTree";

function App() {

  return (
    <div style={{ paddingLeft: 0 }}>
      <RenderTree itemKey="name" value={data} />
    </div>
  );
}

export default App